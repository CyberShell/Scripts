set -euo pipefail
envVars="STALWART_ADMIN_USERNAME:STALWART_ADMIN_PASSWD:STALWART_DOMAIN"

# Variables
GITHUB_FILENAME=stalwart-mail-foundationdb-x86_64-unknown-linux-gnu.tar.gz
GITHUB_RELEASE=$(curl -sL https://api.github.com/repos/stalwartlabs/mail-server/releases | jq -r '.[0].name')
GITHUB_URL="https://github.com/stalwartlabs/mail-server/releases/download/${GITHUB_RELEASE}/${GITHUB_FILENAME}"

rm -rf $GITHUB_FILENAME

# Fetch latest release and extract it
wget "$GITHUB_URL"
tar -xf $GITHUB_FILENAME

# Make backup of data
/opt/stalwart-mail/bin/stalwart-mail --config /opt/stalwart-mail/etc/config.toml --export /opt/stalwart-mail/export

# Stop stalwart
systemctl stop stalwart-mail

# Set permissions for backup
chown -R stalwart-mail:stalwart-mail /opt/stalwart-mail/export

# Replace old binary with new one
mv stalwart-mail /opt/stalwart-mail/bin

# Iteration. use "if ! [ -v $CAR ]" when using Z Shell.
CDR="${envVars}:"; while [ -n "$CDR" ]; do CAR=${CDR%%:*}; if ! [ -v $CAR ]; then echo "Error: Env Variable $CAR is not defined"; exit 1; fi; CDR=${CDR#*:}; done; unset CAR CDR

# Start stalwart-mail service
systemctl start stalwart-mail.service

curl -k -u ${STALWART_ADMIN_USERNAME}:${STALWART_ADMIN_PASSWD} https://${STALWART_DOMAIN}/api/update/webadmin