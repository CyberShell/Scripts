if ! command -v git &> /dev/null
then
    echo "git could not be found... Installing."
    sudo apt-get update; sudo apt-get -y install git
fi
if ! command -v zsh &> /dev/null
then
    echo "zsh could not be found... Installing."
    sudo apt-get update; sudo apt-get -y install zsh
fi

export ZSH="$HOME/.local/oh-my-zsh"
if [ -d "$ZSH" ]; then
  ### Take action if $DIR exists ###
  echo "Installing config files in ${ZSH}..."
else
  ###  Control will jump here if $DIR does NOT exist ###
  echo "Creating ${HOME}/.local..."
  mkdir -v "$HOME/.local"
fi
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended

curl https://gitea.com/CyberShell/Scripts/raw/branch/master/zshrc > "${HOME}"/.zshrc

git clone https://github.com/zsh-users/zsh-autosuggestions "${ZSH}"/plugins/zsh-autosuggestions

git clone https://github.com/zsh-users/zsh-syntax-highlighting.git "${ZSH}"/plugins/zsh-syntax-highlighting

chsh -s "$(which zsh)"
