#!/bin/bash

apt install unbound unbound-host

curl -o /var/lib/unbound/root.hints https://www.internic.net/domain/named.cache

cat << EOF > /etc/unbound/unbound.conf
# Unbound configuration file for Debian.
#
# See the unbound.conf(5) man page.
#
# See /usr/share/doc/unbound/examples/unbound.conf for a commented
# reference config file.
#
# The following line includes additional configuration files from the
# /etc/unbound/unbound.conf.d directory.
server:
  interface: 127.0.0.1
  interface: ::1
  port: 5353

  do-ip6: yes
  prefer-ip6: yes
  do-ip4: yes
  do-udp: yes
  do-tcp: yes

  # Set number of threads to use
  num-threads: 4

  # Log settings
  log-queries: no
  log-replies: no
  log-servfail: no
  log-local-actions: no
  logfile: /dev/null

  # Hide DNS Server info
  hide-identity: yes
  hide-version: yes

  # Limit DNS Fraud and use DNSSEC
  harden-algo-downgrade: yes
  harden-below-nxdomain: yes
  harden-large-queries: yes
  harden-glue: yes
  harden-dnssec-stripped: yes
  harden-referral-path: yes
  harden-short-bufsize: yes
  use-caps-for-id: yes
  qname-minimisation: yes
  deny-any: yes
  aggressive-nsec: yes
  rrset-roundrobin: yes
  minimal-responses: yes

  # If DNSSEC isnt working uncomment the following line
#  auto-trust-anchor-file: "/var/lib/unbound/root.key"

  # Minimum lifetime of cache entries in seconds
  cache-min-ttl: 1800

  # Configure TTL of Cache
  cache-max-ttl: 3600

  # Optimizations
  msg-cache-slabs: 8
  rrset-cache-slabs: 8
  infra-cache-slabs: 8
  key-cache-slabs: 8
  outgoing-range: 8192
  num-queries-per-thread: 4096
  serve-expired: yes
  serve-expired-ttl: 3600
  edns-buffer-size: 1232
  prefetch: yes
  prefetch-key: yes
  target-fetch-policy: "3 2 1 1 1"
  unwanted-reply-threshold: 10000000
  so-sndbuf: 4m
  so-rcvbuf: 4m
  so-reuseport: yes
  # Set cache size
  rrset-cache-size: 2048m
  msg-cache-size: 1024m
  neg-cache-size: 4m

  # increase buffer size so that no messages are lost in traffic spikes
  so-rcvbuf: 1m

  # Private Networks
  private-address: 192.168.0.0/16
  private-address: 169.254.0.0/16
  private-address: 172.16.0.0/12
  private-address: 10.0.0.0/8
  private-address: fd00::/8
  private-address: fe80::/10
EOF

chown -R unbound:unbound /var/lib/unbound

systemctl enable --now unbound