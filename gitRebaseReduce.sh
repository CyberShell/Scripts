cd ~/Repositories/hostfiles;

# Save the current state of the branch in a couple of other branches
git branch beforeReset
git branch verification

# Also mark where we want to start squashing commits
git branch oldBase f12a090a95cf60cf0dde31f683cb4105a30176e8

# Temporarily remove the most recent commits from the current branch,
# because we don't want to squash those:
git reset --hard oldBase

# Using a soft reset to the root commit will keep all of the changes
# staged in the index, so you just need to amend those changes to the
# root commit:
git reset --soft 749a53933c575e240444044ed92a248cf68b8c36
git commit --amend

# Rebase onto the new amended root,
# starting from oldBase and going up to beforeReset
git rebase --onto master oldBase beforeReset
# Switch back to master and (fast-forward) merge it with beforeReset
git checkout master
git merge beforeReset
# Verify that master still contains the same state as before all of the resets
git diff verification
# Cleanup
git branch -D beforeReset oldBase verification
# As part of cleanup, since the original poster mentioned that
# he has a lot of commits that he wants to remove to reduce
# the size of his repo, garbag
git gc --prune=all